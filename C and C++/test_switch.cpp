#include <iostream>
#include <string>
#include <cctype>
using namespace std;

int main(){
    char grade;
    cout << "Enter a grade please: ";
    cin >> grade;
    grade = toupper(grade); // Converts character to uppercase.

    switch (grade)
    {
    case 'A':
        cout << "You got an A!"<< endl;
        break;

    case 'B':
        cout <<"You got a B"<< endl;
        break;
    case 'C':
        cout <<"You got a C"<< endl;
        break;

    default:
        cout <<"You failed!"<< endl;
        break;
    }
    return 0;
}