/**
 * @file Exercise3.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #4 Section 8
 * 
 */

#include <stdio.h>
#include <iostream>

using namespace std;

int main(){
    int digit1,digit2,multi3=0,multi5=0;
    cout << "Enter two numbers : ";
    cin >> digit1 >> digit2;
    cout << endl;

    for (int num = digit1; num <= digit2; ++num)
    {
        if (num % 3 == 0){
            ++multi3;
        }
        else if (num % 5 == 0){
            ++multi5;
        }
    }
    cout << "The number of multiples of 3 is " << multi3 << endl;
    cout << "The number of multiples of 5 is " << multi5 << endl;
    return 0;
    
}