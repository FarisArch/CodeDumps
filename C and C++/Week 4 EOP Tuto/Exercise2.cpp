/**
 * @file Exercise3.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #4 Section 8
 * 
 */

#include <stdio.h>
#include <iostream>

using namespace std;

int main(){
    int end,score;
    while (end != -1){
        cout << "Enter your score: ";
        cin >> score;
        cout << endl;
        if (score >= 60){
            cout << "You pass the exam" << endl;
    }
        else if (score == -1){
            end = -1;
            cout << "No numbers are entered except -1" << endl;
        }
        else{
            cout << "You don't pass the exam." << endl;
        }
    }
    return 0;
}