/**
 * @file Exercise3.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #4 Section 8
 * 
 */

void number(int);

#include <stdio.h>
#include <iostream>

using namespace std;

int main(){
    int digit,mycall;
    cout << "Enter an 4 digit number:";
    cin >> digit;
    cout << endl;
    number(digit);
    

    return 0;
}

void number(int digit){
    int first,second,third,last,sum;
    first = digit / 1000; // 3 
    second = (digit % 1000) / 100; // 4
    third = (digit % 100) / 10; // 5
    last = (digit % 10);
    cout << "The individual digits are  " << first << " " << second << " " << third << " " << last << endl;
    sum = first + second + third + last;
    cout << "The sum of the total individual digits is " << sum << endl;
    return;
}