#include <iostream>

using namespace std;

int main()
{
    float length,width,area,perimeter;
    cout << "Program to compute and output the perimeter and area of rectangle." << endl;
    cout << "Enter values for width and length :";
    cin >> width >> length;
    area = length * width;
    perimeter = 2 * (length + width);

    cout << "Length = " << length << endl;
    cout << "Width = " << width << endl;
    cout << "Perimeter = " << perimeter << endl;
    cout << "Area = " << area << endl;

    return 0;
}