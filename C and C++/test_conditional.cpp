#include <string>
#include <iostream>

using namespace std;

int main(){
    int a,b;
    cout << "Enter number for a and b: ";
    cin >> a >> b;
    cout << endl;
    string max = (a > b) ? "A is bigger than B" : "B is bigger than A"; // If true evaluate Expression 2 
    cout << max << endl;
    return 0;
}