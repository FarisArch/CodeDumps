/*
@author Mohamad Faris Aiman Bin Mohd Faizal
Matric No : 2111809
Lab #1 Section 8
*/
#include <iostream>

using namespace std;

int main(){
    double fahrenheit,celcius;
    cout << "Enter a degree in Celcius: ";
    cin >> celcius;
    // fahrenheit = (celcius * 1.8) + 32; Sanity check.
    fahrenheit = 1.8 * celcius + 32; // Convert (9/5) to 1.8, 9/5 is giving wrong calculations.
    cout << celcius << " Celcius is " << fahrenheit << " Fahrenheit" << endl;

    return 0;
}