/*
@author Mohamad Faris Aiman Bin Mohd Faizal
Matric No : 2111809
Lab #1 Section 8
*/
#include <iostream>

using namespace std;

int main(){
    int number,first,second,third,fourth;
    cout << "Enter a number between 0 and 1000:";
    cin >> number;
    first = (number / 100);
    second =(number / 10) % 10;
    third = (number % 10);
    cout << "The sum of the digits is " << first+second+third << endl;

}
// Test case 932 pass
// 932 % 10 = 2
// 932 / 10 = 93
// 93 % 10 = 3
// 932 / 100 = 9

// Test case 999 pass

