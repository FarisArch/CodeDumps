/*
@author Mohamad Faris Aiman Bin Mohd Faizal
Matric No : 2111809
Lab #1 Section 8
*/
#include <iostream>

using namespace std;

int main(){
    double subTotal,gratuityRate,total,calculatedRate;
    cout << "Enter the subtotal and gratuity rate: ";
    cin >> subTotal >> gratuityRate;
    calculatedRate = (gratuityRate / 100 ) * subTotal;
    total = subTotal + calculatedRate;
    cout << "The gratuity is " << " $" << calculatedRate << " and total is " << " $" << total << endl;

    return 0;
}