/*
@author Mohamad Faris Aiman Bin Mohd Faizal
Matric No : 2111809
Lab #1 Section 8
*/
#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
using namespace std;

int main(){
    string movieName;
    double adultPrice,childPrice,ticketSoldA,ticketSoldC,percentCharity,totalTicketSold,amtSoldA,amtSoldC,grossAmt,amtDonate,netSale;

// PROMPT
    cout << "Enter the movie name ";
    getline(cin,movieName);
    // cin >> movieName;
    cout << "Enter price for adult and children.";
    cin >> adultPrice >> childPrice;
    cout << "Enter the number of tickets sold for adults and children";
    cin >> ticketSoldA >> ticketSoldC;
    cout << "Percentage to be donated to charity?";
    cin >> percentCharity;

// COMPUTE
    totalTicketSold = ticketSoldA + ticketSoldC;
    amtSoldA = adultPrice * ticketSoldA;
    amtSoldC = childPrice * ticketSoldC;
    grossAmt = amtSoldA + amtSoldC;
    amtDonate = (percentCharity/100) * grossAmt;
    netSale = grossAmt - amtDonate;

// DISPLAY
    cout << "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*" << endl;
    cout << "Movie Name: " << movieName << endl;
    cout << "Number of Tickets Sold: " << totalTicketSold << endl;
    cout << "Gross Amount: $ " << grossAmt << endl;
    cout << "Percentage of Gross" << endl;
    cout << "Amount Donated: " << percentCharity <<"%" << endl;
    cout << "Amount Donated: $" << amtDonate << endl;
    cout << "Net Sale: $" << netSale << endl;
    return 0;
}