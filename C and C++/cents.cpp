/*
@author Mohamad Faris Aiman Bin Mohd Faizal
Matric No : 2111809
Lab #1 Section 8
*/
#include <iostream>

using namespace std;
// penny = 1 nickel= 0.05 quarter=0.25 half-dollar = 0.5 
int main(){
    int cents,change,nickels,quarter,pennies,halfDollar;
    cout << "Enter your cents : ";
    cin >> cents;
    halfDollar = cents / 50; // Get amount of dollars 483 : 9
    change = cents % 50; // Now get the amount of change/reminder 483 : 33
    quarter = change / 25;
    change = change % 25;
    nickels = change / 5;
    change = change % 5;
    cout << "Amount of Half-Dollars: "<<  halfDollar << endl;
    cout << "Amount of Quarters: "<<quarter << endl;
    cout << "Amount of Nickels: "<< nickels << endl;
    cout << "Amount of Pennies: "<< change << endl;
}
// Test case 483 half = 33