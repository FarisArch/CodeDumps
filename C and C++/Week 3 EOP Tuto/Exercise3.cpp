/**
 * @file Exercise3.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #3 Section 8
 * 
 */

#include <iostream>
#include <string>
using namespace std;
int main(){
    int day,elapsed;
    string dayoftheweek;
    cout << "Enter today's day: ";
    cin >> day;
    cout << "Enter number of days elapsed since today: ";
    cin >> elapsed;
    cout << endl;
    switch (day)
    {
    case 0:
        dayoftheweek = "Sunday";
        break;
    case 1:
        dayoftheweek = "Monday";
        break;
    case 2:
        dayoftheweek = "Tuesday";
        break;
    case 3:
        dayoftheweek = "Wednesday";
        break;
    case 4:
        dayoftheweek = "Thursday";
        break;
    case 5:
        dayoftheweek = "Friday";
        break;
    case 6:
        dayoftheweek = "Satuday";
        break;
    }
    if (elapsed == 0){
        cout << "Today is " << dayoftheweek << " and the future day is Sunday" << endl;
    }
    else if (elapsed == 1){
        cout << "Today is " << dayoftheweek << " and the future day is Monday" << endl;
    }
    else if (elapsed == 2){
        cout << "Today is " << dayoftheweek << " and the future day is Tuesday" << endl;
    }
    else if (elapsed == 3){
        cout << "Today is " << dayoftheweek << " and the future day is Wednesday" << endl;
    }
    else if (elapsed == 4){
        cout << "Today is " << dayoftheweek << " and the future day is Thursday" << endl;
    }
    else if (elapsed == 5){
        cout << "Today is " << dayoftheweek << " and the future day is Friday" << endl;
    }
    else if (elapsed == 6){
        cout << "Today is " << dayoftheweek << " and the future day is Satuday" << endl;
    }
return 0;
}

// 0 Sun, 1 Mon, 2 Tues, 3 Wed, 4 Thurs , 5 Fri ,6 Sat