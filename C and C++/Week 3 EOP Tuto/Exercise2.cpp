/**
 * @file Exercise2.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #3 Section 8
 * 
 */

#include <iostream>

using namespace std;

int main(){
    int hours,fee;
    cout << "Enter number of hours: ";
    cin >> hours;
    cout << endl;
    if (hours >= 0 && hours <= 3){
        fee = 5;
    }
    else if (hours > 3 && hours <= 9){
        fee = 6 * (hours + 1);
    }
    else{
        fee = 60;
    }
    cout << "Your parking fee is RM" << fee << endl;
    return 0;
}

// Fee 5 if between 0 and 3 including
// Fee 6 * int(h+1) if bigger than 3 and 9 including
// Fee 60 if bigger than 9 and 24 including