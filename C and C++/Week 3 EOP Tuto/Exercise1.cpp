/**
 * @file Exercise1.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #3 Section 8
 * 
 */

#include <iostream>

using namespace std;

int main(){
    int x,y,z,temp1,temp2,temp3;
    cout << "Enter 3 three numbers please : ";
    cin >> x >> y >> z;
    if (x > y){
        temp1 = x;
        temp2 = y;
        temp3 = temp1;
    }

    if (x <= y){
        cout << z << " " << x << " " << y << endl;

    }
    else if (x <= z && z <= y){
        cout << x << " "<< z << " " << y << endl;
    }
    else{
        cout << x << " " << y << " " << z << endl;
    }
    return 0;
}