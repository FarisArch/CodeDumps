/**
 * @file Assignment1.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @Assignment 1
 * 
 */

#include <stdio.h>
#include <iostream>

using namespace std;

int main(){
    int r=0,s=0,t=0;
    for (int row = 0; row <= 9 ; ++row){
        cout << "Row " << row << endl;
        cout << "Enter value for r: ";
        cin >> r;
        cout << endl;
        cout << "Enter value for s: ";
        cin >> s;
        cout << endl;
        cout << "Enter value for t: ";
        cin >> t;
        cout << endl;
        // Can combine all if statements but I want it to be readable.
        if (r!=0  && r !=1){ // R not 0 and 1
            cout << "Invalid input! " << endl;
            break;
        }
        else if (s!=0  && s !=1){ // S not 0 and 1
            cout << "Invalid input! " << endl;
            break;
        }
        else if (t!=0  && t!=1){ // T not 0 and 1
            cout << "Invalid input! " << endl;
            break;
        }
        else {
            cout << "Result of truth value expression :" << endl;
            cout << "De Morgan's Law proven: " << endl;
            cout << "!(r && s) == (!r || !s) ---> (" << !(r && s) << "==" << (!r || !s) << ")\n" << endl;
            // cout << "!( r && s) = " << (!(r && s)) << endl; Not sure if to include this part or not.
            // cout << "( !r || !s) = " << (!r || !s) << endl; 
            cout << "(!r || (s && t))= " << (!r || (s && t)) << endl;
            cout << "((r && s) || !t)= " << ((r && s) || !t) << endl;
        }


    }

    return 0;
}