/**
 * @file Exercise1.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #2 Section 8
 * 
 */
#include <iostream>
#include <iomanip>
#include <cmath>

const double pound = 2.2;
using namespace std;

int main()
{
    double weight;
    cout << "Enter weight in kilogram:";
    cin >> weight;
    cout << fixed  << setprecision(2) << "Weight in pounds is " << weight * pound << endl;
    return 0;
}