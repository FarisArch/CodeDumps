/**
 * @file Exercise2.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #2 Section 8
 * 
 */
#include <iostream>
#include <iomanip>
#include <cmath>

// length = 1.5 * width
// rectangle = 2 * (length + width)
//           = 2 * (1.5 * width + width)
//           = 5 width;
using namespace std;
int main()
{
    double length,width,wireLength;
    cout << "Enter length for picture frame ";
    cin >> wireLength;
    cout << endl;

    width = wireLength / 5;
    length = 1.5 * width;
    cout << "Frame length is " <<length<<endl;
    cout << "Frame width is " << width << endl;

    return 0;
}