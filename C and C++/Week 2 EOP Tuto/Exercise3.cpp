/**
 * @file Exercise3.cpp
 * @author Mohamad Faris Aiman Bin Mohd Faizal
 * @matric no: 2111809
 * @lab #2 Section 8
 * 
 */
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
    int hour,minutes;
    double carA,carB,time,distance;
    cout << "Enter average speed for car A and car B:";
    cin >> carA >> carB;
    cout << "Enter the elapsed time in hour and minutes ";
    cin >> hour >> minutes;
    time = hour + (minutes / 60);
    distance = sqrt(pow(carA * time,2)) + sqrt(pow(carB * time, 2));
    cout << "The shortest distance between the car  is " << fixed << setprecision(2) << distance << endl;

    return 0;
}