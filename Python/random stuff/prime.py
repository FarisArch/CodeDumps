import math

def main(num):
    k = math.sqrt(num)
    kRounded = round(k)
    if num == 2:
        return f"{str(num)} is not prime." # Checks If num is equal to 2, if true not prime
    if num % 2 == 0:
        return f"{str(num)} is not prime." # Checks if num is divisible by 2, if true not prime

    for i in range(1,num,2):               # Checks if num is divisible by any numbers in range from (1-k]. If divisible and variable i is not1, it is not prime.
        if num % i == 0 and i != 1:
            return f"{str(num)} is not prime."
        else:
            return f"{str(num)} is prime"
            break
    ##
    